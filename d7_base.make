;d7_base.make
; $Id$
;
; UO Base Drupal 7 Makefile
; ----------------
; This is the base UO Drupal 7 makefile for creating platforms.


; Core version
; ------------

core = 7.x

; API version
; ------------
; Every makefile needs to declare it's Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.

; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = "7.74"

; Projects
; --------

; Editing and WYSIWYG
projects[ctools] = "1.17"
projects[diff] = "3.4"
projects[imagecache_actions] = "1.12"
projects[image_resize_filter] = "1.16"
; We pull a specific commit for imagecrop since it doesn't have a stable release that supports drupal 7.20+
libraries[imagecrop][download][type]= "git"
libraries[imagecrop][download][url] = "https://git.drupal.org/project/imagecrop.git"
libraries[imagecrop][download][revision] = "98d1faa"
libraries[imagecrop][destination] = "modules"
projects[insert] = "1.4"
projects[file_entity] = "2.30"
projects[linkit] = "3.5"
projects[media] = "2.26"
projects[menu_block] = "2.8"
projects[oembed] = "1.0-rc2"
projects[page_title] = "2.7"
projects[wysiwyg] = "2.6"

; CCK
projects[email] = "1.3"
projects[filefield_sources] = "1.11"
projects[smart_trim] = "1.6"

; Feeds
projects[feeds] = "2.0-beta5"
projects[feeds_tamper] = "1.2"
projects[job_scheduler] = "2.0"

; Misc
projects[acl] = "1.2"
projects[advanced_help] = "1.6"
projects[auto_nodetitle] = "1.0"
projects[backup_migrate] = "3.9"
projects[calendar] = "3.6"
projects[captcha] = "1.7"
projects[colorbox] = "2.15"
projects[custom_search] = "1.20"
projects[css_injector] = "1.10"
projects[datatables] = "1.2"
projects[date] = "2.11-rc1"
projects[date_ical] = "3.10"
projects[ds] = "2.16"
projects[entity] = "1.9"
projects[entityreference] = "1.5"
projects[entityqueue] = "1.5"
projects[features] = "2.12"
projects[field_collection] = "1.1"
projects[field_conditional_state] = "2.1"
projects[field_group] = "1.6"
projects[field_permissions] = "1.1"
projects[find_content] = "1.7"
projects[jquery_update] = "2.7"
projects[google_analytics] = "2.6"
projects[honeypot] = "1.26"
projects[libraries] = "2.5"
projects[lightbox2] = "2.12"
projects[link] = "1.7"
projects[linkchecker] = "1.4"
projects[manualcrop] = "1.7"
projects[markdown] = "1.6"
projects[menu_target] = "1.7"
projects[module_filter] = "2.2"
projects[pathologic] = "3.1"
projects[pathauto] = "1.3"
projects[pdf] = "1.9"
projects[print] = "2.2"
projects[protected_node] = "1.6"
projects[recaptcha] = "2.3"
projects[redirect] = "1.0-rc3"
projects[responsive_menus] = "1.7"
projects[rules] = "2.12"
projects[scanner] = "1.1"
projects[search_api] = "1.26"
projects[search_api_db] = "1.7"
projects[search_api_multi] = "1.5"
projects[shib_auth] = "4.5"
projects[simplesamlphp_auth] = "2.x-dev"
projects[simplify] = "3.4"
projects[site_verify] = "1.2"
projects[stringoverrides] = "1.8"
projects[strongarm] = "2.0"
projects[taxonomy_dupecheck] = "1.2"
projects[token] = "1.7"
projects[transliteration] = "3.2"
projects[uif] = "1.5"
projects[view_unpublished] = "1.2"
projects[views] = "3.24"
projects[views_bulk_operations] = "3.6"
projects[views_data_export] = "3.2"
projects[views_slideshow] = "3.10"
projects[webform] = "4.23"
projects[xmlsitemap] = "2.6"

;jquery cycle - REQUIRED for views_slideshow
libraries[jquery.cycle][type] = "libraries"
libraries[jquery.cycle][download][type] = "file"
libraries[jquery.cycle][download][url] = "https://raw.githubusercontent.com/malsup/cycle/3.0.3-a/jquery.cycle.all.js"

;json2 - REQUIRED for views_slideshow
libraries[json2][type] = "libraries"
libraries[json2][download][type] = "file"
libraries[json2][download][url] = "https://raw.githubusercontent.com/douglascrockford/JSON-js/master/json2.js"

; if we enable content access we need to verify the requirement for forcing "private" filesystem and the ramifications
projects[content_access] = "1.2-beta3"

;Theming
projects[context] = "3.10"
projects[delta] = "3.0-beta11"
projects[omega] = "4.4"
projects[omega_tools] = "3.0-rc4"
projects[zen] = "5.6"

; CKEditor
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.14.0/ckeditor_4.14.0_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; Widgets plugin for ckeditor
libraries[widget][download][type]= "get"
libraries[widget][download][url] = "http://download.ckeditor.com/widget/releases/widget_4.14.0.zip"
libraries[widget][directory_name] = "widget"
libraries[widget][destination] = "libraries/ckeditor/plugins"
; lineutils (dependency of widget) plugin for ckeditor
libraries[lineutils][download][type]= "get"
libraries[lineutils][download][url] = "http://download.ckeditor.com/lineutils/releases/lineutils_4.14.0.zip"
libraries[lineutils][directory_name] = "lineutils"
libraries[lineutils][destination] = "libraries/ckeditor/plugins"

; pdf.js library
libraries[pdf.js][download][type] = "get"
libraries[pdf.js][download][url] = "https://github.com/mozilla/pdf.js/releases/download/v1.1.215/pdfjs-1.1.215-dist.zip"
libraries[pdf.js][directory_name] = "pdf.js"
libraries[pdf.js][destination] = "libraries"

; iCanlendar library
libraries[iCalcreator][download][type] = "get"
libraries[iCalcreator][download][url] = "https://github.com/iCalcreator/iCalcreator/archive/e3dbec2cb3bb91a8bde989e467567ae8831a4026.zip"
libraries[iCalcreator][directory_name] = "iCalcreator"
libraries[iCalcreator][destination] = "libraries"

; datatables library
libraries[datatables][download][type] = "git"
libraries[datatables][download][url] = "git://github.com/DataTables/DataTables.git"
libraries[datatables][download][branch] = "master"
libraries[datatables][destination] = "libraries"

; Manualcrop, imagesLoaded.
libraries[jquery.imagesloaded][download][type] = file
libraries[jquery.imagesloaded][download][url] = https://github.com/desandro/imagesloaded/archive/v2.1.2.tar.gz
libraries[jquery.imagesloaded][download][subtree] = imagesloaded-2.1.2

; Manualcrop, imgAreaSelect.
libraries[jquery.imgareaselect][download][type] = file
libraries[jquery.imgareaselect][download][url] = https://github.com/odyniec/imgareaselect/archive/v0.9.11-rc.1.tar.gz
libraries[jquery.imgareaselect][download][subtree] = imgareaselect-0.9.11-rc.1

