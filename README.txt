Old file names are now tags.

After deploying a platform on to production (with the same naming convention as the following) create a new tag with the following naming convention:

d<major version>.<minor version>_<type>-<internal major version>.<internat minor version>

IE: d7.27_base-1.0